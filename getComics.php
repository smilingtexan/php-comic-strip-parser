<?php
	function save_file($url, $filename, $new_file)
	{
		$date_str = strtoupper(date("d-M-Y"));
		$foldername = "";                       // TODO: be sure to put a valid path on the server here
		$old_path = $foldername . $filename;
		$new_path = $foldername . $new_file;
		
		echo "Getting: {$url}\n";
		if (!file_exists($foldername)) { exec("mkdir -pv {$foldername}"); }
		if (file_exists($old_path)) { exec("rm {$old_path}"); }
		
		exec("wget --no-check-certificate -q -O $old_path $url");

		echo "Moving file {$filename} to {$new_file}\n";
		if (file_exists($new_path)) { exec("rm {$new_path}"); }
		exec("mv {$foldername}{$filename} {$foldername}{$new_file}");
	}
	
	function parse_arcamax_website($url, $name)
	{
		$c_page = curl_init();
		curl_setopt($c_page, CURLOPT_URL, $url);
		curl_setopt($c_page, CURLOPT_HEADER, false);
		curl_setopt($c_page, CURLOPT_NOBODY, false);
		curl_setopt($c_page, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c_page, CURLOPT_FOLLOWLOCATION, true);
		$my_html = curl_exec($c_page);
		$my_code = curl_getinfo($c_page, CURLINFO_HTTP_CODE);
		curl_close($c_page);
		
		$img_start = (strpos($my_html, "<img id=\"comic-zoom\" data-zoom-image=\"") + 38);
		$img_url = substr($my_html, $img_start);
		$img_end = strpos($img_url, "\" src=\"https");
		$img_url = substr($img_url, 0, $img_end);
		
		$file_start = (strrpos($img_url, "/") + 1);
		$filename = substr($img_url, $file_start);
		$ext_start = strpos($filename, ".");
		$extension = substr($filename, $ext_start);
		$new_file = $name . $extension;
		
		save_file($img_url, $filename, $new_file);
	}
	
	function parse_comicskingdom_website($url, $name)
	{
		$c_page = curl_init();
		curl_setopt($c_page, CURLOPT_URL, $url);
		curl_setopt($c_page, CURLOPT_HEADER, true);
		curl_setopt($c_page, CURLOPT_NOBODY, false);
		curl_setopt($c_page, CURLOPT_TIMEOUT, 5);
		curl_setopt($c_page, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c_page, CURLOPT_FOLLOWLOCATION, true);
		$my_html = curl_exec($c_page);
		$my_code = curl_getinfo($c_page, CURLINFO_HTTP_CODE);
		curl_close($c_page);
		
		if ($my_html === false)
		{
			echo "No page info retrieved for {$url} <br/> I'm not sure what went wrong.<br/>";
			print_r($my_html);
			print_r($my_code);
		}
		
		$img_start = (strpos($my_html, "<meta property=\"og:image\" content=\"") + 35);
		$img_url = substr($my_html, $img_start);
		$img_end = strpos($img_url, "\" />");
		$img_url = substr($img_url, 0, $img_end);
		
		$file_start = (strrpos($img_url, "file=") + 5);
		$filename = substr($img_url, $file_start);
		$ext_start = strpos($filename, ".");
		if ($ext_start === false) { $extension = ".gif"; }
		else { $extension = substr($filename, $ext_start); }
		$new_file = $name . ".png";

		$c_page = curl_init();
		curl_setopt($c_page, CURLOPT_URL, $img_url);
		curl_setopt($c_page, CURLOPT_HEADER, false);
		curl_setopt($c_page, CURLOPT_NOBODY, false);
		curl_setopt($c_page, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c_page, CURLOPT_FOLLOWLOCATION, true);
		$my_html = curl_exec($c_page);
		$my_code = curl_getinfo($c_page, CURLINFO_HTTP_CODE);
		curl_close($c_page);
		$foldername = "";                           // TODO: be sure to put a valid server directory here
		$old_path = $foldername . $filename;
		$new_path = $foldername . $name . ".*";
		
		echo "Getting: {$url}\n";
		if (!file_exists($foldername)) { exec("mkdir -pv {$foldername}"); }
		if (file_exists($old_path)) { exec("rm {$old_path}"); }
		
		exec("rm {$new_path}");		// remove all old images regardless of filename ($name + ".*")
		$new_path = $foldername . $new_file;		// make sure we put the correct extension back
		file_put_contents($new_path, $my_html);		// this saves the file; no need to call the save_file function
	}
	
	function parse_gocomics_website($url, $name)
	{
		$c_page = curl_init();
		curl_setopt($c_page, CURLOPT_URL, $url);
		curl_setopt($c_page, CURLOPT_HEADER, false);
		curl_setopt($c_page, CURLOPT_NOBODY, false);
		curl_setopt($c_page, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c_page, CURLOPT_FOLLOWLOCATION, true);
		$my_html = curl_exec($c_page);
		$my_code = curl_getinfo($c_page, CURLINFO_HTTP_CODE);
		curl_close($c_page);
		
		$error = (strpos($my_html, "role=\"alert\""));
		if ($error == false)
		{
			$img_start = (strpos($my_html, "<meta property=\"og:image\" content=\"") + 35);
			$img_url = substr($my_html, $img_start);
			$img_end = strpos($img_url, "\" />");
			$img_url = substr($img_url, 0, $img_end);
			
			$file_start = (strrpos($img_url, "/") + 1);
			$filename = substr($img_url, $file_start);
			$ext_start = strpos($filename, ".");
			if ($ext_start === false) { $extension = ".png"; }
			else { $extension = substr($filename, $ext_start); }
			$new_file = $name . ".png";
			
			save_file($img_url, $filename, $new_file);
		}
		else
		{
			$pos = strpos($url, ".com/") + 5;
			$new_url = substr($url, 0, strpos($url, "/", $pos));	// make sure to start searching after 'https://'			
			$end =  strpos($new_url, "/", 8);
			$search = substr($new_url, $end);	
			
			$c_page = curl_init();
			curl_setopt($c_page, CURLOPT_URL, $new_url);
			curl_setopt($c_page, CURLOPT_HEADER, false);
			curl_setopt($c_page, CURLOPT_NOBODY, false);
			curl_setopt($c_page, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c_page, CURLOPT_FOLLOWLOCATION, true);
			$my_html = curl_exec($c_page);
			$my_code = curl_getinfo($c_page, CURLINFO_HTTP_CODE);
			curl_close($c_page);
			
			$search_term = "<a class=\"gc-blended-link gc-blended-link--primary\" href=\"{$search}/";
			$img_start = (strpos($my_html, $search_term));
			$img = substr($my_html, $img_start);
			$img_end = strpos($img, ">");
			$img_url = substr($img, 0, $img_end);
			$img_start = (strpos($img_url, "href=\"/"));
			$img_url = substr($img_url, $img_start);
			$img_start = (strpos($img_url, "/", 8));			
			$img_url = substr($img_url, $img_start);
			$img_end = strpos($img_url, "\"");
			$img_url = substr($img_url, 0, $img_end);
			
			$img_url = $new_url . $img_url;
			
			$c_page = curl_init();
			curl_setopt($c_page, CURLOPT_URL, $img_url);
			curl_setopt($c_page, CURLOPT_HEADER, false);
			curl_setopt($c_page, CURLOPT_NOBODY, false);
			curl_setopt($c_page, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($c_page, CURLOPT_FOLLOWLOCATION, true);
			$my_html = curl_exec($c_page);
			$my_code = curl_getinfo($c_page, CURLINFO_HTTP_CODE);
			curl_close($c_page);
			
			$img_start = (strpos($my_html, "<meta property=\"og:image\" content=\"") + 35);
			$img_url = substr($my_html, $img_start);
			$img_end = strpos($img_url, "\" />");
			$img_url = substr($img_url, 0, $img_end);
			
			$file_start = (strrpos($img_url, "/") + 1);
			$filename = substr($img_url, $file_start);
			$ext_start = strpos($filename, ".");
			if ($ext_start === false) { $extension = ".png"; }
			else { $extension = substr($filename, $ext_start); }
			$new_file = $name . ".png";
			
			save_file($img_url, $filename, $new_file);
		}
	}
	
	date_default_timezone_set('America/Chicago');
	$time2update = false;
	$update_file = "";          // TODO: Be sure to put a valid path to an "update file" here
	$dom = date('d');
	$month = date('F');
	$year = date('Y');
	$update = $month . "\n" . $dom . "\n" . $year;
	if (file_exists($update_file))
	{
		$prev_update = explode("\n", file_get_contents($update_file));
		if (($year != $prev_update[2]) || ($month != $prev_update[0]) || ($dom != $prev_update[1]))
		{
			$time2update = true;
		}
	}
	else
	{
		$time2update = true;
	}
	
	if ($time2update)
	{
		/*	
		parse_arcamax_website("https://www.arcamax.com/thefunnies/beetlebailey/", "beetlebailey");	
		parse_arcamax_website("https://www.arcamax.com/thefunnies/blondie/", "blondie");
		parse_arcamax_website("https://www.arcamax.com/thefunnies/bizarro/", "bizarro");
		parse_arcamax_website("https://www.arcamax.com/thefunnies/dennisthemenace/", "dennisthemenace");
		parse_arcamax_website("https://www.arcamax.com/thefunnies/familycircus/", "familycircus");	
		parse_arcamax_website("https://www.arcamax.com/thefunnies/hagarthehorrible/", "hagarthehorrible");	
		parse_arcamax_website("https://www.arcamax.com/thefunnies/hiandlois/", "hiandlois");	
		parse_arcamax_website("https://www.arcamax.com/thefunnies/mallardfillmore/", "mallardfillmore");	
		parse_arcamax_website("https://www.arcamax.com/thefunnies/zits/", "zits");
		*/
		parse_comicskingdom_website("https://comicskingdom.com/beetle-bailey-1", "beetlebailey");
		parse_comicskingdom_website("https://comicskingdom.com/blondie", "blondie");
		parse_comicskingdom_website("https://comicskingdom.com/bizarro", "bizarro");
		parse_comicskingdom_website("https://comicskingdom.com/dennis-the-menace", "dennisthemenace");
		parse_comicskingdom_website("https://comicskingdom.com/family-circus", "familycircus");	
		parse_comicskingdom_website("https://comicskingdom.com/hagar-the-horrible", "hagarthehorrible");
		parse_comicskingdom_website("https://comicskingdom.com/hi-and-lois", "hiandlois");
		parse_comicskingdom_website("https://comicskingdom.com/mallard-fillmore", "mallardfillmore");
		parse_comicskingdom_website("https://comicskingdom.com/zits", "zits");
				
		
		$date_str = date("Y/m/d");
		$dow = date("D");
		
		parse_gocomics_website("https://www.gocomics.com/adamathome/{$date_str}/", "adamathome");
		parse_gocomics_website("https://www.gocomics.com/andertoons/{$date_str}/", "andertoons");
		parse_gocomics_website("https://www.gocomics.com/andycapp/{$date_str}/", "andycapp");
		parse_gocomics_website("https://www.gocomics.com/animalcrackers/{$date_str}/", "animalcrackers");
		parse_gocomics_website("https://www.gocomics.com/arloandjanis/{$date_str}/", "arloandjanis");
		parse_gocomics_website("https://www.gocomics.com/aunty-acid/{$date_str}/", "aunty-acid");
		parse_gocomics_website("https://www.gocomics.com/bc/{$date_str}/", "bc");
		parse_gocomics_website("https://www.gocomics.com/babyblues/{$date_str}/", "babyblues");
		parse_gocomics_website("https://www.gocomics.com/ballardstreet/{$date_str}/", "ballardstreet");
		parse_gocomics_website("https://www.gocomics.com/thebarn/{$date_str}/", "thebarn");
		parse_gocomics_website("https://www.gocomics.com/bignate/{$date_str}/", "bignate");
		parse_gocomics_website("https://www.gocomics.com/broomhilda/{$date_str}/", "broomhilda");
		parse_gocomics_website("https://www.gocomics.com/calvinandhobbes/{$date_str}/", "calvinandhobbes");
		parse_gocomics_website("https://www.gocomics.com/cathy/{$date_str}/", "cathy");
		parse_gocomics_website("https://www.gocomics.com/cattitude-doggonit/{$date_str}/", "cattitude-doggonit");
		parse_gocomics_website("https://www.gocomics.com/crabgrass/{$date_str}/", "crabgrass");
		parse_gocomics_website("https://www.gocomics.com/diamondlil/{$date_str}/", "diamondlil");
		parse_gocomics_website("https://www.gocomics.com/forbetterorforworse/{$date_str}/", "forbetterorworse");
		if ($dow == "Sun") { parse_gocomics_website("https://www.gocomics.com/foxtrot/{$date_str}/", "foxtrot"); }
		else { parse_gocomics_website("https://www.gocomics.com/foxtrotclassics/{$date_str}/", "foxtrotclassics"); }
		parse_gocomics_website("https://www.gocomics.com/frank-and-ernest/{$date_str}/", "frank-and-ernest");
		parse_gocomics_website("https://www.gocomics.com/garfield/{$date_str}/", "garfield");
		parse_gocomics_website("https://www.gocomics.com/heathcliff/{$date_str}/", "heathcliff");
		parse_gocomics_website("https://www.gocomics.com/herman/{$date_str}/", "herman");
		parse_gocomics_website("https://www.gocomics.com/homeandaway/{$date_str}/", "homeandaway");
		parse_gocomics_website("https://www.gocomics.com/inthebleachers/{$date_str}/", "inthebleachers");
		parse_gocomics_website("https://www.gocomics.com/mother-goose-and-grimm/{$date_str}/", "mothergooseandgrimm");
		parse_gocomics_website("https://www.gocomics.com/nancy/{$date_str}/", "nancy");
		parse_gocomics_website("https://www.gocomics.com/peanuts/{$date_str}/", "peanuts");
		parse_gocomics_website("https://www.gocomics.com/peanuts-begins/{$date_str}/", "peanuts-begins");
		parse_gocomics_website("https://www.gocomics.com/pearlsbeforeswine/{$date_str}/", "pearlsbeforeswine");
		parse_gocomics_website("https://www.gocomics.com/pickles/{$date_str}/", "pickles");
		parse_gocomics_website("https://www.gocomics.com/pluggers/{$date_str}/", "pluggers");
		parse_gocomics_website("https://www.gocomics.com/roseisrose/{$date_str}/", "roseisrose");
		parse_gocomics_website("https://www.gocomics.com/shermanslagoon/{$date_str}/", "shermanslagoon");
		parse_gocomics_website("https://www.gocomics.com/shoe/{$date_str}/", "shoe");
		parse_gocomics_website("https://www.gocomics.com/theargylesweater/{$date_str}/", "theargylesweater");
		parse_gocomics_website("https://www.gocomics.com/the-born-loser/{$date_str}/", "the-born-loser");
		parse_gocomics_website("https://www.gocomics.com/wallace-the-brave/{$date_str}/", "wallace-the-brave");
		parse_gocomics_website("https://www.gocomics.com/wizardofid/{$date_str}/", "wizardofid");
		parse_gocomics_website("https://www.gocomics.com/ziggy/{$date_str}/", "ziggy");
		
		file_put_contents($update_file, $update);
	}
	else
	{
		echo "Comics have already been updated for today.\n";
	}
?>
